devops-recruitment-tasks
========================
Task1:

To build your new AMI  you are required to provide the following variables:

"aws_access_key": "",
"aws_secret_key": "",
"vpc_id": "",
"subnet_id": "",
"aws_region" : "eu-west-2"


packer build -var "vpc_id=" -var "subnet_id=" packer.json

Tip: in case there are no VPC created in your aws region you could use the terraform code from task 2 to create one. 
Here is an example:

terraform plan --target=module.network -out="Packerplan"
terrafrom apply "Packerplan"



Task 2 Terraform

For this task to be tested a terraform.tfvars file has to be created with the following variables:


aws_region                      = "eu-west-2"
aws_access_key                  = ""
aws_secret_key                  = ""
keypair_name                    = "Alexandru-London"

# Networking provisioning
network_name                    = "Alexandru"    # generic name for tags used to create network resources. Also used to name S3 bucket
vpc_cidr_block                  = "10.0.0.0/16"  # Assigned VPC CIDR block
subnet_cidr_block               = "10.0.x.0/24"  # Assigned Subnet IPV4 CIDR blocks (move the x char to where you want to have the split for subnets)

accessing_computer_ip           = "0.0.0.0/32" # This could also be set to your public IP address so you can restrict access to resources. 
                                               # Grab your public IP Address by running: curl icanhazip.com

ami_owner_id                    = "724115245423"    # Account owner ID from AMI




Task 3:

To run app: cd app/django_drive
pipenv shell
python manage.py runserver


task not finished.