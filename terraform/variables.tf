
variable "aws_region" {
  type        = string
  description = "AWS Region you want to use."
}

variable "aws_access_key" {
  type        = string
  description = "Your AWS account identification key."
}

variable "aws_secret_key" {
  type        = string
  description = "Your AWS secret key."
}

variable "keypair_name" {
  type        = string
  description = "Network Name"
}

variable "network_name" {
  type        = string
  description = "Network Name"
}

variable "vpc_cidr_block" {
  type        = string
  default     = ""
  description = "VPC CIDR Block"
}

variable "subnet_cidr_block" {
  type        = string
  default     = ""
  description = "SUBNET IPV4 CIDR Block"
}

variable "accessing_computer_ip" {
  type        = string
  description = "Public IP of the computer accessing bastion."
}

variable "ami_owner_id" {
  type        = string
}