module "network" {
    source                             = "./modules/network/"
    aws_region                         = var.aws_region
    aws_access_key                     = var.aws_access_key
    aws_secret_key                     = var.aws_secret_key
    network_name                       = var.network_name
    vpc_cidr_block                     = var.vpc_cidr_block
    subnet_cidr_block                  = var.subnet_cidr_block
}


module "bastion" {
  source = "./modules/bastion/"

  accessing_computer_ip              = var.accessing_computer_ip
  aws_region                         = var.aws_region
  keypair_name                       = var.keypair_name

  // inputs from modules     
  vpc_id                             = module.network.vpc_id
  network_ids                        = module.network.network_ids
}

module "webserver" {
  source = "./modules/webserver/"

  accessing_computer_ip              = var.accessing_computer_ip
  aws_region                         = var.aws_region
  keypair_name                       = var.keypair_name

  // inputs from modules     
  vpc_id                             = module.network.vpc_id
  network_ids                        = module.network.network_ids
  bastion_sg                         = module.bastion.bastion_sg
  ami_owner_id                       = var.ami_owner_id

}

module "s3" {
  source = "./modules/s3/"

  accessing_computer_ip              = var.accessing_computer_ip
  aws_region                         = var.aws_region
  s3_name                            = var.network_name
  
  // inputs from modules     
  
}