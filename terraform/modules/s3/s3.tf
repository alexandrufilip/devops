resource "aws_s3_bucket" "my_s3" {
  bucket = "alexandru"
}

resource "aws_s3_bucket_public_access_block" "my_s3" {
  bucket = "${aws_s3_bucket.my_s3.id}"

  block_public_acls   = true
  block_public_policy = true
}