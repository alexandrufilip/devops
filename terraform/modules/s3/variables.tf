variable "accessing_computer_ip" {
  type        = string
  description = "Public IP of the computer accessing bastion."
}

variable "aws_region" {
  type        = string
  description = "AWS Region you want to use."
}

variable "s3_name" {
  type        = string
}