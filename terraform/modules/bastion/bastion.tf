data "aws_ami" "latest-ubuntu" {
most_recent = true
owners = ["099720109477"] # Canonical

  filter {
      name   = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
      name   = "virtualization-type"
      values = ["hvm"]
  }
}

resource "aws_instance" "bastion" {
  ami           = "${data.aws_ami.latest-ubuntu.id}"
  instance_type = "t2.micro"
  subnet_id                            = var.network_ids.0
  key_name                             = var.keypair_name
  vpc_security_group_ids               = [aws_security_group.terraform-bastion.id]
  associate_public_ip_address          = true
  root_block_device {
    volume_type                        = "gp2"
    delete_on_termination              = true
  }
  tags = {
    "Name"     = "bastion"
  }
}
