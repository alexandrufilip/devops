resource "aws_security_group" "terraform-bastion" {
  name        = "Bastion"
  description = "Security group for Bastion"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name"      = "Bastion"
  }
}

resource "aws_security_group_rule" "terraform-bastion-ssh" {
  cidr_blocks       = [var.accessing_computer_ip]
  description       = "Allow workstation to communicate with bastion"
  from_port         = 0
  protocol          = "tcp"
  security_group_id = aws_security_group.terraform-bastion.id
  to_port           = 22
  type              = "ingress"
}
