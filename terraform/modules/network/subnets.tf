data "aws_availability_zones" "terraform-vnova" {
  state = "available"
}

resource "aws_subnet" "terraform-vnova" {
  count             = length(data.aws_availability_zones.terraform-vnova.names)
  availability_zone = data.aws_availability_zones.terraform-vnova.names[count.index]
  cidr_block        = replace("${var.subnet_cidr_block}", "x", "${count.index}")
  vpc_id            = aws_vpc.terraform-vnova.id

  tags = {
    "Name"          = "${var.network_name}"
  }
}
