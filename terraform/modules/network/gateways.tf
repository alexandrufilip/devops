resource "aws_internet_gateway" "terraform-vnova" {
  vpc_id = aws_vpc.terraform-vnova.id

  tags = {
    Name = var.network_name
  }
}