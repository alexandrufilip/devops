resource "aws_route_table" "terraform-vnova" {
  vpc_id = aws_vpc.terraform-vnova.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terraform-vnova.id
  }
  
  tags = {
    Name = var.network_name
  }
}

resource "aws_route_table_association" "terraform-vnova" {
  count = length(data.aws_availability_zones.terraform-vnova.zone_ids)

  subnet_id      = aws_subnet.terraform-vnova[count.index].id
  route_table_id = aws_route_table.terraform-vnova.id
}