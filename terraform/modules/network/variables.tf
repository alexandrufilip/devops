
variable "aws_region" {
  type        = string
  description = "AWS Region you want to use."
}

variable "aws_access_key" {
  type        = string
  description = "The account identification key used by your Terraform client."
}

variable "aws_secret_key" {
  type        = string
  description = "The secret key used by your terraform client to access AWS."
}

variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
}

variable "subnet_cidr_block" {
  type        = string
  description = "SUBNET IPV4 CIDR Block"
}

variable "network_name" {
  type        = string
  description = "Network Name"
}