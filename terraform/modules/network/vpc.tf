resource "aws_vpc" "terraform-vnova" {
  cidr_block           = "${var.vpc_cidr_block}"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    "Name"             = "${var.network_name}"
  }
}