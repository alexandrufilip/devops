output "vpc_id" {
  value = aws_vpc.terraform-vnova.id
}

output "network_ids" {
  value = aws_subnet.terraform-vnova.*.id
}