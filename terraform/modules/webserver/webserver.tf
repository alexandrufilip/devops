data "aws_ami" "latest-webserver" {
  most_recent = true
  owners = ["${var.ami_owner_id}"] 

    filter {
        name   = "name"
        values = ["packer-ubuntu-exampletest123*"]
    }

    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }
}

resource "aws_instance" "webserver" {
  ami           = "${data.aws_ami.latest-webserver.id}"
  instance_type = "t2.micro"
  subnet_id                            = var.network_ids.0
  key_name                             = var.keypair_name
  vpc_security_group_ids               = [aws_security_group.terraform-webserver.id]
  associate_public_ip_address          = true
  root_block_device {
    volume_type                        = "gp2"
    delete_on_termination              = true
  }
  tags = {
    "Name"     = "webserver"
  }
}
