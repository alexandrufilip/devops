resource "aws_security_group" "terraform-webserver" {
  name        = "Webserver"
  description = "Security group for Webserver"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name"      = "Webserver"
  }
}

resource "aws_security_group_rule" "terraform-webserver-80" {
  description       = "Allow workstations to communicate with the webserver"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 0
  protocol          = "tcp"
  security_group_id = aws_security_group.terraform-webserver.id
  to_port           = 80
  type              = "ingress"
}


resource "aws_security_group_rule" "terraform-bastion-webserver-ssh" {

  description               = "Allow bastion to ssh into webserver"
  from_port                 = 0
  protocol                  = "tcp"
  security_group_id         = aws_security_group.terraform-webserver.id
  source_security_group_id  = var.bastion_sg
  to_port                   = 22
  type                      = "ingress"
}