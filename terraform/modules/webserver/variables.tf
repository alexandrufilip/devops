variable "accessing_computer_ip" {
  type        = string
  description = "Public IP of the computer accessing bastion."
}

variable "ami_owner_id" {
  type        = string
}

variable "vpc_id" {
  type = string
}

variable "network_ids" {
  type        = list(string)
}

variable "aws_region" {
  type        = string
}

variable "keypair_name" {
  type = string
}

variable "bastion_sg" {
  type = string
}