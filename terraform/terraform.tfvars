aws_region                      = "eu-west-2"
aws_access_key                  = ""
aws_secret_key                  = ""
keypair_name                    = "Alexandru-London"

# Networking provisioning
network_name                    = "Alexandru"    # generic name for tags used to create network resources. Also used to name S3 bucket
vpc_cidr_block                  = "10.0.0.0/16"  # Assigned VPC CIDR block
subnet_cidr_block               = "10.0.x.0/24"  # Assigned Subnet IPV4 CIDR blocks (move the x char to where you want to have the split for subnets)

accessing_computer_ip           = "167.98.18.93/32" # This could also be set to your public IP address.
ami_owner_id                    = "724115245423"    # Account owner ID from AMI